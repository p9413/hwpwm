#############################################
# UI Module for Output via the Pico LCD 3.5 #
#############################################

import machine
import picorestouchlcd35base
from passwords import Passwords
import usbout
import re
import time
import math
import sys
import gc

_TK = [ list('          !*#,;'), 
                  list('1234567890?+-_.'),
                  list('QWERTZUIOP=~^%('),
                  list('ASDFGHJKL ){}[]'),
                  list('  YXCVBNM @/\|:') ]

# Max lines count
_ML = 14

MIN_X = 333
MIN_Y = 410
MAX_X = 3860
MAX_Y = 3560

#translate touch pad coordinates
def get_XYPoint(pos):
    X_Point = int((pos[1] - MIN_X) / (MAX_X - MIN_X) * 480)
    Y_Point = 320 - int((pos[0] - MIN_Y) / (MAX_Y - MIN_Y) * 320)
    return [X_Point, Y_Point]
# Translate line number to Y px
def line_to_px(line):
    return 30 + line * 20
# Translate Y px to line number
def px_to_line(y):
    return math.floor((y - 20) / 20)

class UI:
    pos = None
    page = 0
    layer = ''
    items = []

    # initialize display
    def __init__(self):
        self.LCD = picorestouchlcd35base.LCD_3inch5()
        self.LCD.bl_ctrl(100)
    # selected item
    def current_item(self):
        if(self.pos == None):
            return [0, 0x0000]
        return self.items[self.page * _ML + self.pos]
    # fill UP
    def fill_up(self, color = None):
        if(not color ):
            color = self.LCD.WHITE
        self.LCD.fill(color)
        self.LCD.show_up_left()
        self.LCD.show_up_right()
    # fill DOWN
    def fill_down(self, color = None):
        if(not color ):
            color = self.LCD.WHITE
        self.LCD.fill(color)
        self.LCD.show_down_left()
        self.LCD.show_down_right()
    # fill all parts
    def fill_all(self, color = None):
        self.fill_up(color)
        self.fill_down(color)
    # draw all keys and input    
    def draw_keyboard(self, txt = '', shift = False, x = 0, y = 0, key_size = 20, left = True):
        self.LCD.fill(self.LCD.GREEN)
        self.LCD.rect(x, y, 255, 20, self.LCD.BLACK)
        self.LCD.text(txt, x + 5, y + 5, self.LCD.RED)
        row = 0
        for line in _TK:
            col = 0
            if((left and col > 7) or (not left and col < 7)):
                next
            for key in line:
                if(not shift):
                    key = str.lower(key)
                if(not key == ' '):
                    self.draw_button(key, x + col * (key_size + 10) + math.floor(col / 10) * 15,
                                     y + row * (key_size + 10), key_size, key_size) 
                col += 1
            row += 1
        if(left):
            self.draw_button("CAPS", x, y + 4 * key_size + 40, key_size * 2 + 10, key_size)
            self.LCD.show_down_left()
        else:
            self.draw_button("<-", 260 + x, y, int(key_size * 1.5), key_size)
            self.LCD.rect(270 + x, y + 3 * key_size + 30, key_size, key_size * 2 + 10, self.LCD.RED)
            self.LCD.text("RE", 272 + x, y + 3 * key_size + 30 + 5)
            self.LCD.text("TU", 272 + x, y + int(3.5 * key_size) + 35 + 5)
            self.LCD.text("RN", 272 + x, y + 4 * key_size + 40 + 5)
            self.LCD.show_down_right()
        if(left):
            self.draw_keyboard(txt, shift, x - 240, y, key_size, False)
            
    # check if keyboard key is hit
    def get_keyboard_key(self, shift, x = 0, y = 0, key_size = 20):
        key = ''
        pos = self.LCD.touch_get()
        if(pos):
            xy = get_XYPoint(pos)
            row = math.floor((xy[1] - 160 - y) / (key_size + 10))
            col = math.floor(((xy[0] - x ) / (key_size + 10)))
            print(xy)
            print([row, col])
            if(row == 4 and col < 2):
                key = 'CAPS'
            elif(row == 0 and col == 9):
                key = '<-'
            elif(row > 2 and col == 9):
                key = 'RETURN'
            elif(xy[0] > 380 + 10 and xy[1] > line_to_px(2) and
                 xy[0] < 380 + 10 + 7 * 10 and y < line_to_px(2) + 20):
                key = 'Cancel'
            elif(xy[0] > 380 + 10 and xy[1] > line_to_px(4) and
                 xy[0] < 380 + 10 + 7 * 10 and xy[1] < line_to_px(4) + 20):
                key = 'Cancel'
            else:
                try:
                    key = _TK[row][col]
                except:
                    key = ''
        if(not shift):
            key = str.lower(key)
        return key
    # Get string input from user via keyboard
    def get_keyboard_input(self, txt = ''):
        CAPS = True
        while True:
            self.draw_keyboard(txt, CAPS, 10, 10)
            key = self.get_keyboard_key(CAPS, 10, 0)
            if(key == '<-' and key != ''):
                txt = txt[:-1]
            elif(key == 'CAPS'):
                CAPS = not CAPS
            elif(key == 'RETURN'):
                break
            elif(key == 'Cancel'):
                return key
            elif(key):
                txt += key
            time.sleep(0.2)
        return txt
    # draw a button
    def draw_button(self, txt, x, y, sizeX = 20, sizeY = 20):
        self.LCD.rect(x, y, sizeX, sizeY ,self.LCD.RED)
        self.LCD.text(txt, x + round(sizeX / 2) - len(txt) * 4, y + round(sizeY / 2) - 4,self.LCD.BLACK)
    # list print
    def print_list(self, items, list_start, list_end, line = 0):
        out = []
        #line = 0
        for x in range(list_start, list_end):
            item = items[x]
            out.append(item)
            if item[1] == 0x8000:
                self.LCD.text(item[0], 20, line_to_px(line), self.LCD.BLUE)
            else:
                self.LCD.text(item[0] + '/', 20, line_to_px(line), self.LCD.BLUE)            
            line += 1
        return out
    # print a list and nav + buttons | upper screen part
    def print_list_and_nav_up(self, header, items = None, account = True):
        if(not items):
            items = self.items
        # printed items
        out = []
        
        ## Start UP print ##
        self.LCD.fill(self.LCD.WHITE)
        self.LCD.text(header, 8, 10, self.LCD.BLUE)
        
        # File List
        list_start = self.page * _ML
        list_end = min(list_start + math.floor(_ML/2), len(items))
        out = self.print_list(items, list_start, list_end)
        
        # Position
        if(self.pos and self.pos < _ML / 2 - 1):
            self.LCD.fill_rect(10, line_to_px(self.pos) + 1, 5, 5, self.LCD.RED)

        self.LCD.show_up_left()

        # File Info
        self.LCD.fill(self.LCD.WHITE)
        if(account):
            self.LCD.text("Selected Account: ", 0, line_to_px(0), self.LCD.RED)
            if(self.current_item()[1] == 0x8000):
                self.LCD.text(self.current_item()[0], 0 + 10, line_to_px(1), self.LCD.BLUE)
                # Send Buttons
                self.LCD.text('Username:', 0 + 10, line_to_px(2) + 5)
                self.draw_button('Send!', 80 + 10, line_to_px(2), 5 * 10, 20)
                self.draw_button('Change!', 140 + 10, line_to_px(2), 7 * 10, 20)
                self.LCD.text('Password:', 0 + 10, line_to_px(4) + 5)
                self.draw_button('Send!', 80 + 10, line_to_px(4), 5 * 10, 20)
                self.draw_button('Change!', 140 + 10, line_to_px(4), 7 * 10, 20)
            else:
                self.LCD.text("None", 0 + 10, line_to_px(1), self.LCD.BLUE)

        # show
        self.LCD.show_up_right()

        return out

    # print a list and nav + buttons | lower screen part
    def print_list_and_nav_down(self, items = None, menu = True):
        if(not items):
            items = self.items
        # printed items
        out = []
        
        ## Start DOWN print ##
        self.LCD.fill(self.LCD.WHITE)
        
        # File List
        list_start = min(self.page * _ML + math.floor(_ML/2), len(items))
        list_end = min(list_start + math.floor(_ML/2) - 1, len(items))
        out += self.print_list(items, list_start, list_end, - 1)
        
        # Position
        if(self.pos and self.pos >= _ML / 2 - 1):
            self.LCD.fill_rect(10, line_to_px(self.pos) - 160 + 1, 5, 5, self.LCD.RED)
        
        # Footer
        self.LCD.text(f"Page: {self.page + 1} / {self.page_count()}", 10, 160 - 18, self.LCD.BLUE)
        if(self.page > 0):
            self.draw_button('Back', 110, 160 - 25, 20 * 2)
        if(self.page < self.page_count() - 1):
            self.draw_button('Next', 110 + 20 * 2 + 10, 160 - 25, 20 * 2)
        
        # Show
        self.LCD.show_down_left()
        
        self.LCD.fill(self.LCD.WHITE)
        if(menu):
            self.draw_button('MENU', 180, 120, 50, 30)
        self.LCD.show_down_right()
        
        return out
    
    # print Password import fail info
    def print_import_fail(self, filename, reason):
        self.fill_all(self.LCD.WHITE)
        self.LCD.text('Failed import:', 20, 40, self.LCD.BLUE)
        self.LCD.text(filename, 20, 60, self.LCD.BLUE)
        self.LCD.text(reason, 20, 80, self.LCD.BLUE)
        self.LCD.show_up_left()
    # last page
    def page_count(self):
        return math.ceil(len(self.items) / _ML)
    # touch input for navigation
    def get_input(self):
        xy = None
        pos = self.LCD.touch_get()
        if(pos):
            xy = get_XYPoint(pos)
        return xy
    # navigate through filelist
    def navigate(self, shown_items):
        xy = self.get_input()
        if(xy):
            line = px_to_line(xy[1])
            file = Passwords.BASE_DIR + self.layer + str(self.current_item()[0])
            if(xy[0] < 240):
                if(line >= 0 and line < len(shown_items)):
                    # Directory
                    if(shown_items[line][1] == 0x4000):
                        self.layer += shown_items[line][0] + "/"
                        self.pos = None
                        self.page = 0
                    # ..
                    elif(shown_items[line][1] == 0x0000):
                        self.layer = re.sub('\/[^\/]*\/$', '/', self.layer)
                        self.pos = None
                        self.page = 0
                    else:
                        self.pos = line
                if(xy[0] > 105 and xy[0] < 155 and xy[1] > 130 and self.page > 0):
                    self.page -= 1
                    self.pos = None
                if(xy[0] > 155 and xy[0] < 205 and xy[1] > 130 and self.page < self.page_count()):
                    self.page += 1
                    self.pos = None
            
            elif(xy[0] > 320 and xy[0] < 380):
                if(line == 2):
                    if(self.current_item()[1] == 0x8000):
                        usbout.send_str(Passwords.get_user(file))
                elif(line == 4):
                    if(self.current_item()[1] == 0x8000):
                        usbout.send_str(Passwords.get_password(file))
            elif(self.current_item() != [0,0]):
                if(line == 2):
                    self.LCD.fill(self.LCD.WHITE)
                    self.LCD.text('Change Username for: ' + self.current_item()[0], 20, 20, self.LCD.BLUE)
                    self.LCD.show_up_left()
                    self.LCD.fill(self.LCD.WHITE)
                    self.LCD.fill_rect(140 + 10, line_to_px(2), 70, 20, self.LCD.RED)
                    self.LCD.text('Cancel', 140 + 10 + 8, line_to_px(2) + 6, self.LCD.WHITE)
                    self.LCD.show_up_right()
                    new_user = self.get_keyboard_input(Passwords.get_user(file))
                    if(new_user != 'Cancel'):
                        Passwords.set_user(file, new_user)
                elif(line == 4):
                    self.LCD.fill(self.LCD.WHITE)
                    self.LCD.text('Change Password for: ' + self.current_item()[0], 20, 20, self.LCD.BLUE)
                    self.LCD.show_up_left()
                    self.LCD.fill(self.LCD.WHITE)
                    self.LCD.fill_rect(140 + 10, line_to_px(4), 70, 20, self.LCD.RED)
                    self.LCD.text('Cancel', 140 + 10 + 8, line_to_px(4) + 6, self.LCD.WHITE)
                    self.LCD.show_up_right()
                    new_password = self.get_keyboard_input(Passwords.get_password(file))
                    if(new_password != 'Cancel'):
                        Passwords.set_password(file, new_password)
            elif(xy[0] > 420 and xy[1] > 280):
                self.settings_menu()
                self.layer = '/'
                self.pos = None
                self.page = 0
                
    # login UI
    def login(self):
        self.fill_all()
        self.LCD.text('Welcome to HWPWM!', 20, 20, self.LCD.BLUE)
        self.LCD.text('Please enter login key.', 20, 40, self.LCD.BLUE)
        self.LCD.show_up_left()
        
        in_key = self.get_keyboard_input()

        if(not in_key or not Passwords.set_and_verify_key(in_key)):
            self.fill_all()
            self.LCD.text('FAILED!', 20, 80, self.LCD.BLUE)
            self.LCD.show_up_left()
            sys.exit()

        Passwords.impor(self)
        
    # Main UI
    def run(self):
        self.layer = '/'
        self.pos = None
        self.page = 0

        while True:
            if(self.layer != '/'):
                self.items = [['..', 0x0000]]
            else:
                self.items = []
            self.items += Passwords.list(Passwords.BASE_DIR + self.layer)
            
            shown_files = self.print_list_and_nav_up('DIR: ' + self.layer)
            shown_files += self.print_list_and_nav_down()
            
            key = self.navigate(shown_files)

            time.sleep(0.1)

    # Settings Menu
    def settings_menu(self):
        self.layer = '/'
        self.pos = None
        self.page = 0

        while True:
            self.items = [ ['Change Keyboard Layout', 0x8000],
                           ['Back to List', 0x8000],
                           ['Quit', 0x8000 ],
                           ]
            
            header = 'Settings: ' + self.layer
            
            shown_files = self.print_list_and_nav_up(header, self.items, False)
            shown_files += self.print_list_and_nav_down(self.items, False)
            
            xy = self.get_input()
            if(xy):
                line = px_to_line(xy[1])
                if(line == 0):
                    self.keyboard_layout_menu()
                elif(line == 1):
                    return True
                elif(line == 2):
                    machine.reset()

            time.sleep(0.1)
  
    # Keyboard Layout Menu
    def keyboard_layout_menu(self):
        self.layer = '/'
        self.pos = None
        self.page = 0

        while True:
            current_layout = usbout.get_keyboard_layout()
            
            self.items = [['German', 0x8000],
                          ['English', 0x8000],
                          ['Back', 0x8000 ],
                          ]
            
            for i, item in enumerate(self.items):
                if(item[0] == current_layout):
                    self.items[i][0] = item[0] + ' (active)'
                    break
            
            header = 'Settings: ' + self.layer
            
            shown_files = self.print_list_and_nav_up(header, self.items, False)
            shown_files += self.print_list_and_nav_down(self.items, False)
            
            xy = self.get_input()
            if(xy):
                line = px_to_line(xy[1])
                # select menu item
                if(line == 0):
                    usbout.set_keyboard_layout('German')
                elif(line == 1):
                    usbout.set_keyboard_layout('English')
                elif(line == 2):
                    return True

            time.sleep(0.1)