######################
# Main Script entry  #
######################
#
# BEGIN UI module choose
#
# use EITHER:
#
#import picorestouchlcd35sdcard
#picorestouchlcd35sdcard.init_sdcard()
#from picorestouchlcd35 import UI
#
# OR:
from picolcd114 import UI
#
########################
# END UI module choose #
########################

# Initialize UI
ui = UI()

# Login
ui.login()

# Main Loop
ui.run()
