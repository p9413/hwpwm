# HWPWM

Hardware Password Manager with Raspberry Pi Pico

## Required Hardware
* Raspberry Pi Pico
* Any supported Display module (see Supported Hardware)

## Supported Hardware
* Pico-LCD-1.14 (Waveshare)
* Pico-ResTouch-LCD-3.5 (Waveshare)

## Installation
1) deploy firmware.uf2 to Raspberry Pico, it contains adapted micropython
2) upload all python files (except password example) to /
3) upload all mpy files from lib folder to the Pico /lib
3) create dirs /secret and /pw_in on the Pico
4) Add your credentials to the example password file and upload it to the Pico /pw_in
5) reset the Pico

## Roadmap
- Network module for the 1.14 variant
