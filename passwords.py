################################
# Module for Password handling #
################################

import os
import re
import cryptolib
import time
from random import randint

from helper import chomp

class Passwords:
    CRYPTO_KEY = ''
    BASE_DIR = '/secret/'
    
    # read line #line from a file and remove the line end
    @staticmethod
    def read_file(filename, line):
        file = open(filename, "rb")
        for _ in range(line):
            file.readline()
        line = file.readline()
        file.close()
        return chomp(line)
    # save user/PW
    @classmethod
    def _save_us_pw(cls, file, username, password):
        outfile = open(file, 'wb')
        outfile.write(cls.my_encrypt(username) + "\n" + cls.my_encrypt(password))
        outfile.close()
    # read a username from a password entry file
    @classmethod
    def get_user(cls, file):
        return cls.my_decrypt(cls.read_file(file, 0)).rstrip('\x00')
    # save new username
    @classmethod
    def set_user(cls, file, username):
        cls._save_us_pw(file, username, cls.get_password(file))
    # read a password from a password entry file
    @classmethod
    def get_password(cls, file):
        return cls.my_decrypt(cls.read_file(file, 1)).rstrip('\x00')
    # save new password
    @classmethod
    def set_password(cls, file, password):
        cls._save_us_pw(file, cls.get_user(file), password)
    # get a file / password list from current_dir
    @staticmethod
    def list(dir):
        return list(os.ilistdir(dir))
    
    # generate a password
    @staticmethod
    def generate(length):
        alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        alphanum = '0123456789'
        alphasoz = '!*#,;?+-_.=~^%(){}[]|:/'
        alphaall = alphabet + alphanum + alphasoz
        
        while True:
            password = ''.join(alphaall[randint(0, len(alphaall) - 1)] for x in range(length))
            if(re.search('[a-zA-Z]*', password) and
               re.search('[0-9]*', password) and
               re.search(f"[{alphasoz}]*", password)):
                break
        return password
    
    # import passwords
    @classmethod
    def impor_folder(cls, ui, folder):
        if(folder[-1] == '/'):
            folder = folder[:-1]
        files = os.ilistdir(folder)
        folder += '/'
        for file in files:
            infile = open(folder + file[0], 'r')
            eof = False
            while(not eof):
                filename = chomp(infile.readline())
                if(filename):
                    target_file = cls.BASE_DIR + filename
                    target_path = re.sub('\/[^\/]*$', '', target_file)
                    username = chomp(infile.readline())
                    password = chomp(infile.readline())
                    if(password[0] == "P"):
                        password = password[1:]
                    else:
                        password = cls.generate(int(password[1:]))
                    
                    if(len(username) < 6 or len(password) < 6):
                        ui.print_import_fail(file[0], 'Too short!')
                        time.sleep(5)
                    else:
                        try:
                            os.stat(target_path)
                        except:
                            os.mkdir(target_path)
                        
                        cls._save_us_pw(target_file, username, password)                        
                else:
                    eof = True
                
            infile.close
            os.remove('/pw_in/' + file[0])
    
    # import passwords
    @classmethod
    def impor(cls, ui):
        cls.impor_folder(ui, '/pw_in/')
        cls.impor_folder(ui, '/sd/pw_in')
    # encrypt a string
    @classmethod
    def my_encrypt(cls, st):
        crypt = cryptolib.aes(cls.CRYPTO_KEY, 1)
        data_bytes = st.encode()
        return crypt.encrypt(data_bytes + b'\x00' * ((16 - (len(data_bytes) % 16)) % 16))

    # decrypt a string
    @classmethod
    def my_decrypt(cls, st):
        crypt = cryptolib.aes(cls.CRYPTO_KEY, 1)
        out = ''
        try:
            out = crypt.decrypt(st).decode()
        except:
            out = 'error'
        return out

    # validate the login key and set it as CRYPTOKEY
    @classmethod
    def set_and_verify_key(cls, key):
        cls.CRYPTO_KEY = key + 'a' * ((16 - (len(key) % 16)) % 16)

        new = False
        try:
            key_file = open('/.key','rb')
            saved_key = key_file.read()
            key_file.close()
        except:
            key_file = open('/.key','wb')
            key_file.write(cls.my_encrypt(cls.CRYPTO_KEY))
            key_file.close()
            new = True

        return (new or cls.CRYPTO_KEY == cls.my_decrypt(saved_key))