##############################################
# UI Module for Output via the Pico LCD 1.14 #
##############################################

import machine
import lcdclass
import re
import time
import math
from passwords import Passwords
import usbout
import sys

class UI:
    MAX_LINES = 5
    pos = 0
    page = 0
    layer = ''
    items = []

    def __init__(self):
        # LCD init
        self.pwm = machine.PWM(machine.Pin(13))
        self.pwm.freq(1000)#Screen refresh rate
        self.pwm.duty_u16(32000)#Screen Brightness, max 65535
        self.LCD = lcdclass.LCD_1inch14()

        # Keys init
        self.key_A = machine.Pin(15, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_B = machine.Pin(17, machine.Pin.IN, machine.Pin.PULL_UP)

        self.key_up = machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_mid = machine.Pin(3, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_left = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_down = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_right = machine.Pin(20, machine.Pin.IN, machine.Pin.PULL_UP)

    # Translate line number to Y px
    @staticmethod
    def line_to_px(line):
        return 30 + line * 18

    # login UI
    def login(self):
        self.LCD.fill(self.LCD.white)
        self.LCD.text('Welcome to HWPWM!', 20, 20, self.LCD.blue)
        self.LCD.text('Please enter login key:', 20, 40, self.LCD.blue)
        self.LCD.show()

        in_key = ''
        wait_for_key = True
        while wait_for_key:
            key = self.get_key()
            if(key == 'Mid'):
                wait_for_key = False
            elif(key != ''):
                in_key += key[0]
                self.LCD.text('*' * len(in_key), 20, 60, self.LCD.blue)
                self.LCD.show()
            time.sleep(0.1)
            
        if(not Passwords.set_and_verify_key(in_key)):
            self.LCD.text('FAILED!', 20, 80, self.LCD.blue)
            self.LCD.show()
            sys.exit()
            
        Passwords.impor(self)

    # print a list
    def print_list(self, header, items):
        
        self.LCD.fill(self.LCD.white)
        self.LCD.text(header, 8, 10, self.LCD.blue)
        
        line = 0
        out = []
        
        for x in range(self.page * 5, min(self.page * 5 + 5, len(items))):
            item = items[x]
            if(line < self.page * 5 and line > (self.page * 5 + 3)):
                next
            out.append(item)
            
            if item[1] == 0x8000:
                self.LCD.text(item[0], 20, self.line_to_px(line), self.LCD.blue)
            else:
                self.LCD.text(item[0] + '/', 20, self.line_to_px(line), self.LCD.blue)            
            line += 1
        
        self.LCD.fill_rect(10, self.line_to_px(self.pos) + 1, 5, 5, self.LCD.red)
        self.LCD.text(f"Page: {self.page + 1} / {self.page_count()}", 10, self.line_to_px(5), self.LCD.blue)
        self.LCD.show()
        return out

    # print Password import fail info
    def print_import_fail(self, filename, reason):
        self.LCD.fill(self.LCD.white)
        self.LCD.text('Failed import:', 20, 40, self.LCD.blue)
        self.LCD.text(filename, 20, 60, self.LCD.blue)
        self.LCD.text(reason, 20, 80, self.LCD.blue)
        self.LCD.show()

    # read a key press & release
    def get_key(self):
        key = ''
        if(self.key_A.value() == 0):
            key = 'A'
            button = self.key_A
        if(self.key_B.value() == 0):
            key = 'B'
            button = self.key_B
        if(self.key_up.value() == 0):
            key = 'Up'
            button = self.key_up
        if(self.key_down.value() == 0):
            key = 'Down'
            button = self.key_down
        if(self.key_left.value() == 0):
            key = 'Left'
            button = self.key_left
        if(self.key_right.value() == 0):
            key = 'Right'
            button = self.key_right
        if(self.key_mid.value() == 0):
            key = 'Mid'
            button = self.key_mid
        if(key != ''):
            wait_for_key_stop = True
            while wait_for_key_stop:
                if(button.value() == 1):
                    wait_for_key_stop = False

        return key            

    # highest page
    def page_count(self):
        return math.ceil(len(self.items) / self.MAX_LINES)

    # general navigation handling
    def navigate(self, shown_items, key):
        # Scroll
        if(key == 'Down'):
            if(self.pos < len(shown_items) - 1 and self.pos < self.MAX_LINES - 1):
                self.pos += 1
                key = ''
            elif(self.page < self.page_count() - 1):
                self.pos = 0
                self.page += 1
                key = ''
            elif(self.page == self.page_count() - 1 and self.pos == len(shown_items) - 1):
                 self.pos = 0
                 self.page = 0
                 key = ''

        if(key == 'Up'):
            if self.pos > 0:
                self.pos -= 1
                key = ''
            elif(self.page > 0):
                self.pos = self.MAX_LINES - 1
                self.page -= 1
                key = ''
            elif(self.pos == 0 and self.page == 0):
                self.pos = (len(self.items) -1 ) % self.MAX_LINES
                self.page = self.page_count() - 1
        
        # Follow path
        if(key == 'Right' or key == 'Mid'):
            if(shown_items[self.pos][1] == 0x4000):
                self.layer += shown_items[self.pos][0] + "/"
                self.pos = 0
                self.page = 0
                key = ''

        # Go back in path
        if(key == 'Left'):
            if(self.layer != '/'):
                self.layer = re.sub('\/[^\/]*\/$', '/', self.layer)
                self.pos = 0
                self.page = 0
                key = ''
                
        return key

    # Main UI
    def run(self):
        self.layer = '/'
        self.pos = 0
        self.page = 0

        while True:
            self.items = Passwords.list(Passwords.BASE_DIR + self.layer)
            
            header = 'DIR: ' + self.layer
            
            shown_files = self.print_list(header, self.items)
            
            key = self.get_key()
            
            key = self.navigate(shown_files, key)
        
            # open menu
            if(key == 'Left' and self.layer == '/'):
                self.settings_menu()
                    
            # send username
            if(key == 'A'):
                if(shown_files[self.pos][1] == 0x8000):
                    usbout.send_str(Passwords.get_user(Passwords.BASE_DIR + self.layer + shown_files[self.pos][0]))
                
            # send password
            if(key == 'B'):
                if(shown_files[self.pos][1] == 0x8000):
                    usbout.send_str(Passwords.get_password(Passwords.BASE_DIR + self.layer + shown_files[self.pos][0]))
                    
            time.sleep(0.1)
    
    # Settings Menu
    def settings_menu(self):
        self.layer = '/'
        self.pos = 0
        self.page = 0

        while True:
            self.items = [ ['Change Keyboard Layout', 0x8000],
                           ['Back to List', 0x8000],
                           ['Quit', 0x8000 ],
                           ]
            
            header = 'Settings: ' + self.layer
            
            shown_files = self.print_list(header, self.items)
            
            key = self.get_key()

            key = self.navigate(shown_files, key)
        
            # select menu item
            if(key == 'A'):
                if(shown_files[self.pos][0] == 'Change Keyboard Layout'):
                    self.keyboard_layout_menu()
                elif(shown_files[self.pos][0] == 'Back to List'):
                    self.pos = 0
                    return True
                elif(shown_files[self.pos][0] == 'Quit'):
                    machine.reset()

            time.sleep(0.1)

    # Keyboard Layout Menu
    def keyboard_layout_menu(self):
        self.layer = '/'
        self.pos = 0
        self.page = 0

        while True:
            current_layout = usbout.get_keyboard_layout()
            
            self.items = [['German', 0x8000],
                          ['English', 0x8000],
                          ['Back', 0x8000 ],
                          ]
            
            for i, item in enumerate(self.items):
                if(item[0] == current_layout):
                    self.items[i][0] = item[0] + ' (active)'
                    break
            
            header = 'Settings: ' + self.layer
            
            shown_files = self.print_list(header, self.items)
            
            key = self.get_key()

            key = self.navigate(shown_files, key)
        
            # select menu item
            if(key == 'A'):
                if(shown_files[self.pos][0] == 'German'):
                    usbout.set_keyboard_layout('German')                    
                elif(shown_files[self.pos][0] == 'English'):
                    usbout.set_keyboard_layout('English')
                elif(shown_files[self.pos][0] == 'Back'):
                    self.pos = 0
                    return True

            time.sleep(0.1)