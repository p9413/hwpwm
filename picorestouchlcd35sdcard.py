#######################################
# SD Card Module for the Pico LCD 3.5 #
#######################################

import machine
import sdcard
import uos

# initalize the SD Card and mount it
def init_sdcard():
    cs = machine.Pin(22, machine.Pin.OUT)
    spi = machine.SPI(1,
                      baudrate=1000000,
                      polarity=0,
                      phase=0,
                      bits=8,
                      firstbit=machine.SPI.MSB,
                      sck=machine.Pin(10),
                      mosi=machine.Pin(11),
                      miso=machine.Pin(12))
    sd = sdcard.SDCard(spi, cs)
    vfs = uos.VfsFat(sd)
    uos.mount(vfs, "/sd")