#############################
# Module for USB HID output #
#############################

import keyboard
import re

MOD_ALT_GR = 0xe6
MOD_LEFT_SHIFT  = 0xe1

HID = keyboard.Keyboard()

_DEKB = { '@' : [0x14, MOD_ALT_GR], 'z' : [0x1c, 0], 'Z' : [0x1c, MOD_LEFT_SHIFT],
          'y' : [0x1d, 0], 'Y' : [0x1d, MOD_LEFT_SHIFT], '!' : [0x1e, MOD_LEFT_SHIFT],
          '$' : [0x21, MOD_LEFT_SHIFT], '%' : [0x22, MOD_LEFT_SHIFT], '/' : [0x24, MOD_LEFT_SHIFT],
          '{' : [0x24, MOD_ALT_GR], '(' : [0x25, MOD_LEFT_SHIFT], '[' : [0x25, MOD_ALT_GR],
          ')' : [0x26, MOD_LEFT_SHIFT], ']' : [0x26, MOD_ALT_GR], '=' : [0x27, MOD_LEFT_SHIFT],
          '}' : [0x27, MOD_ALT_GR], '?' : [0x2d, MOD_LEFT_SHIFT], "\\" : [0x2d, MOD_ALT_GR],
          '+' : [0x30, 0], '*' : [0x30, MOD_LEFT_SHIFT], '~' : [0x30, MOD_ALT_GR],
          '#' : [0x31, 0], "'" : [0x31, MOD_LEFT_SHIFT], '#' : [0x32, 0], "'" : [0x32, MOD_LEFT_SHIFT],
          ',' : [0x36, 0], ';' : [0x36, MOD_LEFT_SHIFT], '.' : [0x37, 0], ':' : [0x37, MOD_LEFT_SHIFT],
          '-' : [0x38, 0], '_' : [0x38, MOD_LEFT_SHIFT], '|' : [0x64, MOD_ALT_GR],
        }

_ENKB = { '!' : [0x1e, MOD_LEFT_SHIFT], '@' : [0x1f, MOD_LEFT_SHIFT], '$' : [0x21, MOD_LEFT_SHIFT],
          '%' : [0x22, MOD_LEFT_SHIFT], '{' : [0x2f, MOD_LEFT_SHIFT], '/' : [0x38, 0],
          '(' : [0x26, MOD_LEFT_SHIFT], '[' : [0x2f, 0], ')' : [0x27, MOD_LEFT_SHIFT],
          ']' : [0x30, 0], '=' : [0x2e, 0], '}' : [0x30, MOD_LEFT_SHIFT],
          '?' : [0x38, MOD_LEFT_SHIFT], "\\" : [0x31, 0], '+' : [0x2e, MOD_LEFT_SHIFT],
          '*' : [0x25, MOD_LEFT_SHIFT], '~' : [0x32, MOD_LEFT_SHIFT], '#' : [0x20, MOD_LEFT_SHIFT],
          "'" : [0x34, 0], ',' : [0x36, 0], ';' : [0x33, 0], '.' : [0x37, 0],
          ':' : [0x33, MOD_LEFT_SHIFT], '-' : [0x2d, 0], '_' : [0x2d, MOD_LEFT_SHIFT],
          '|' : [0x31, MOD_LEFT_SHIFT],
         }

# Scancode / Charmap
def CHARMAP():

    if(get_keyboard_layout() == 'German'):
        return _DEKB
    if(get_keyboard_layout() == 'English'):
        return _ENKB

# change keyboard layout
def set_keyboard_layout(layout):
    config_file = open('/.keyboard_layout',"w")
    config_file.write(layout)
    config_file.close()

## get configured keyboard layout
def get_keyboard_layout():
    try:
        config_file = open('/.keyboard_layout',"r")
        current_layout = config_file.read()
        config_file.close()
    except:
        current_layout = 'German'
    
    return current_layout

# send a key-code via USB HID
def send_key( mod, key):
    HID.press(mod,key)
    HID.release(mod, key)

# send a whole string via USB HID
def send_str(st):
    for char in st:
        if char in CHARMAP():
            send_key(CHARMAP()[char][1], CHARMAP()[char][0])
        else:
            if re.search('[a-z]', char):
                send_key(0, 0x04 + ord(char) - ord('a'))
            if re.search('[A-Z]', char):
                send_key(MOD_LEFT_SHIFT, 0x04 + ord(char) - ord('A'))
            if char == '0':
                send_key(0, 0x27)
            if re.search('[1-9]', char):
                send_key(0, 0x1e + ord(char) - ord('1'))